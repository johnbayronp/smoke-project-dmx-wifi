#include <ESP8266WiFi.h>
#include <EEPROM.h>
#include <string.h>
#include "esp.h"

esp esp1;

const char SEPARATOR = '/';
byte RGB[3] = {0,0,0};

void setup(){
  Serial.begin(9600);
  Serial.println();
  esp1.startParammeters();
}

void loop(){
  if(esp1.listenClient()){
    readClient(esp1.dataReceived());
    esp1.stopClient();
  }
}

void readClient(String data){
  byte one = data.indexOf(SEPARATOR);
  String option = data.substring(0, one);
  String information = data.substring(one + 1);
  if(option == "NEWRGB"){
    newRGB(information);
  }else if(!esp1.anOptionEspSelected()){
    esp1.printClient("opcion no valida|");
  }
}

void newRGB(String colors){
  String print = "";
  for(int i = 0; i < 3; i++){
    byte one = colors.indexOf(SEPARATOR);
    RGB[i] = colors.substring(0, one).toInt();
    print += (String)(RGB[i]);
    colors = colors.substring(0, one);
  }
  esp1.printClient(print);
}
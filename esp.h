#include <ESP8266WiFi.h>
#include <EEPROM.h>
#include <string.h>
#include <WiFiUdp.h>
#include <ESP8266WebServer.h>

class esp{
  private:
    const char *ntpServerName = "time.nist.gov", SEPARATOR = '|';
    static const int NTP_PACKET_SIZE = 48;     // NTP time stamp is in the first 48 bytes of the message
    unsigned int localPort = 2390;      // local port to listen for UDP packets
    byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets
    unsigned long sendNTPpacket(IPAddress &address);

    boolean espConfigured = false;
    byte Stop = 0, hours = 0, minutes = 0, seconds = 0, ntpDia = 0;
    String notice, newNotice, defaultKey = "12345";
    char ssid[21], pass[21], key[6], host[16],softAPSsid[10], softAPPass[10];
    int port;
    void wifiConnect();
    void saveWifiData();
    void ReadClientData();
    void saveKey();
    void readEEPROM();
    void cofigureSoftAp ();
    void saveHost();
    void printWifiData();
  public:
    esp();
    boolean anOptionEspSelected();
    boolean listenClient();
    void startParammeters();
    void EEPROMFinish();
    boolean Key();
    String dataReceived();
    void stopClient();
    void printClient(String dataForPrint);
    void clearEEPROM();
    void EEPROMWritelong(int address, long value);
    long EEPROMReadlong(long address); 
    void connectToHost(String message);  
    void getTime();
    int getHour();
    int getMinutes();
    int getSeconds(); 
    int getDay(); 
    boolean conectedToWireless();
};

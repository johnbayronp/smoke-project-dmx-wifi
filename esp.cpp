#include <ESP8266WiFi.h>
#include <EEPROM.h>
#include <string.h>
#include <WiFiUdp.h>
#include <ESP8266WebServer.h>
#include "esp.h"

IPAddress timeServerIP; // time.nist.gov NTP server address
WiFiUDP udp;                        // A UDP instance to let us send and receive packets over UDP
WiFiClient client;
WiFiClient newClient;
WiFiServer server(6789);

esp::esp (){
}

void esp::startParammeters(){// inicia la red wifi y lee los datos de coneccion a una punto de acceso
    esp::readEEPROM();
    if (softAPSsid[0] == '\0' || softAPPass[0] == '\0'){
        const String defaultSsidAP = "MYWIFILIB";
        const String defaultPassAP = "123456789";
        defaultSsidAP.toCharArray(softAPSsid, 11);
        defaultPassAP.toCharArray(softAPPass, 11);
    }
    WiFi.softAP(softAPSsid, softAPPass); //name and password of the network
    server.begin();
    udp.begin(localPort);
    if (key[0] == '\0'){
       defaultKey.toCharArray(key, 6);
    }
    if (ssid[0] != '\0' || pass[0] != '\0'){
        esp::wifiConnect();
    }
    client = server.available();
    Stop = 0;
}

boolean esp::listenClient(){//no se queda esperando al que el cliente envie los datos
    if (client){
        if (client.available()){//le los datos del cliente
            notice = client.readStringUntil('~');
            esp::ReadClientData();
            client.flush();
            Stop = 1;
            return true;
        }
        else if (!client){//si el cliente se desconecta
            client.stop();
            Stop = 0;
        }
        return false;
    }else {
        client = server.available();
        return false;
    }
}

void esp::printClient(String dataForPrint){
    client.print(dataForPrint);
}

void esp::stopClient(){
    if (Stop == 1){
        client.stop();
        Stop = 0;
    }
}

boolean esp::Key() {//si los datos de contrasenia son correctos devuelve true
    if (notice.substring (0,  notice.indexOf(SEPARATOR)) == key){
        return true;
    } else {
        return false;
    }
}

String esp::dataReceived() {//retorna la informacion recibida
    if (esp::Key()){
        return notice.substring(notice.indexOf(SEPARATOR) + 1);
    }else{return "";}
}

void esp::readEEPROM(){//lee los datos guardados
    EEPROM.begin(512);
    EEPROM.get(0, ssid);
    EEPROM.get(21, pass);
    EEPROM.get(43, key);
    EEPROM.get(49, host);
    port = esp::EEPROMReadlong(65);
    EEPROM.get(70, softAPSsid);
    EEPROM.get(80, softAPPass);
    esp::EEPROMFinish();   
}

void esp::ReadClientData(){//analiza si el cliente pide un cambio de clave o un cambio de red
    byte one = notice.indexOf(SEPARATOR);
    String option = notice.substring (one + 1, one + 11);
    newNotice = notice.substring (one + 11);
    if (esp::Key()){
        espConfigured = true;
        if (option == "WIFI.CONF|"){
            esp::saveWifiData();
        }
        else if (option == "CHANGEKEY|"){
            esp::saveKey();
        }
        else if (option == "CHANGHOST|"){
            esp::saveHost();
        }
        else if (option == "NEWSOFTAP|"){
            esp::cofigureSoftAp ();
        }
        else if (option == "SHOWMEDAT|"){
            esp::printWifiData();
        }else{
            espConfigured = false;
        }
    }else{
        client.print ("clave incorrecta|");
        espConfigured = true;
    } 
}

void esp::saveWifiData(){//guarda los datos de la nueva red wifi 
    String buffer;
    (newNotice.substring(0, newNotice.indexOf(SEPARATOR))).toCharArray(ssid, 21);
    (newNotice.substring(newNotice.indexOf(SEPARATOR) + 1)).toCharArray(pass, 21);
    buffer = newNotice.substring(0, newNotice.indexOf(SEPARATOR)) + SEPARATOR;
    buffer += (newNotice.substring(newNotice.indexOf(SEPARATOR) + 1) + SEPARATOR);
    client.print(buffer);
    client.stop();
    Stop = 0;
    WiFi.disconnect(true);
    esp::wifiConnect();     
}

void esp::saveKey() {//guarda la nueva clave
    (newNotice.substring(0, 5)).toCharArray(key, 6),
    client.print((newNotice.substring(0, 5)) + SEPARATOR);
    EEPROM.begin(512);
    EEPROM.put(43, key);
    esp::EEPROMFinish();
}

void esp::saveHost(){
    String buffer; 
    (newNotice.substring(0, newNotice.indexOf(SEPARATOR))).toCharArray(host, 16);
    port = (newNotice.substring(newNotice.indexOf(SEPARATOR) + 1)).toInt();
    buffer = (newNotice.substring(0, newNotice.indexOf(SEPARATOR)) + SEPARATOR);
    buffer += ((newNotice.substring(newNotice.indexOf(SEPARATOR) + 1)) + SEPARATOR);
    client.print(buffer);
    EEPROM.begin(512);
    EEPROM.put(49, host);
    esp::EEPROMWritelong(65, port);
    EEPROMFinish();
}

void esp::wifiConnect(){ //start connection to a WiFi network
  byte timeOut = 0;
  WiFi.begin(ssid, pass);
  while (WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print(".");
    yield();
    if (timeOut > 60){
      WiFi.mode(WIFI_AP);
      break;
    }
    timeOut++;
  }
  if (WiFi.status() == WL_CONNECTED){
    WiFi.mode(WIFI_AP_STA);
    Serial.print("conectado a la red");
    EEPROM.begin(512);
    EEPROM.put(0, ssid);
    EEPROM.put(21, pass);
    esp::EEPROMFinish();
  }
}

void esp::connectToHost(String message){
  if (newClient.connect(host, port)){
    newClient.print(message);
    newClient.stop();
    delay(100);
    Serial.println("me conecte");
  }
}

void esp::cofigureSoftAp (){
    String buffer; 
    if((newNotice.substring(newNotice.indexOf(SEPARATOR) + 1)).length() > 9 || 
      (newNotice.substring(0, newNotice.indexOf(SEPARATOR))).length() > 9){
        client.print("los datos deben contener maximo 9 caracteres|");
    }else{
      (newNotice.substring(0, newNotice.indexOf(SEPARATOR))).toCharArray(softAPSsid, 11);
      (newNotice.substring(newNotice.indexOf(SEPARATOR) + 1)).toCharArray(softAPPass, 11);
      buffer = (newNotice.substring(0, newNotice.indexOf(SEPARATOR))) + SEPARATOR;
      buffer += (newNotice.substring(newNotice.indexOf(SEPARATOR) + 1)) + SEPARATOR;
      client.print(buffer);
      EEPROM.begin(512);
      EEPROM.put(70, softAPSsid);
      EEPROM.put(80, softAPPass);
      esp::EEPROMFinish();
    }
}

void esp::printWifiData(){
    String print = ssid;
    print += SEPARATOR;
    print += host;
    print += SEPARATOR;
    print += port;
    print += SEPARATOR;
    print += softAPSsid;
    print += SEPARATOR;
    client.print (print);
}

boolean esp::anOptionEspSelected(){
   return espConfigured;
}

void esp::EEPROMFinish(){
    EEPROM.commit();
    EEPROM.end();
}

void esp::clearEEPROM(){//borra los datos de la eprom de la posicion 60 en adelante
  EEPROM.begin(512);
  for (int i = 90; i <= EEPROM.length(); i++){
    EEPROM.write(i, 0);
  }
  esp::EEPROMFinish();
  client.print("memoria borrada|");
}

void esp::EEPROMWritelong(int address, long value){
  //Decomposition from a long to 4 bytes by using bitshift.
  //One = Most significant -> Four = Least significant byte
  byte four = (value & 0xFF);
  byte three = ((value >> 8) & 0xFF);
  byte two = ((value >> 16) & 0xFF);
  byte one = ((value >> 24) & 0xFF);
  //Write the 4 bytes into the eeprom memory.
  EEPROM.write(address, four);
  EEPROM.write(address + 1, three);
  EEPROM.write(address + 2, two);
  EEPROM.write(address + 3, one);
}

long esp::EEPROMReadlong(long address){
  //Read the 4 bytes from the eeprom memory.
  long four = EEPROM.read(address);
  long three = EEPROM.read(address + 1);
  long two = EEPROM.read(address + 2);
  long one = EEPROM.read(address + 3);
  //Return the recomposed long by using bitshift.
  return ((four << 0) & 0xFF) + ((three << 8) & 0xFFFF) + ((two << 16) & 0xFFFFFF) + ((one << 24) & 0xFFFFFFFF);
}

void esp::getTime(){
  //get a random server from the pool
  WiFi.hostByName(ntpServerName, timeServerIP);
  sendNTPpacket(timeServerIP); // send an NTP packet to a time server
  // wait to see if a reply is available
  delay(1000);
  int cb = udp.parsePacket();
  if (!cb){
    Serial.println("no packet yet");
  }
  else{
    Serial.print("packet received, length=");
    Serial.println(cb);
    // We've received a packet, read the data from it
    udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer
    //the timestamp starts at byte 40 of the received packet and is four bytes,
    // or two words, long. First, esxtract the two words:
    unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
    unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
    // combine the four bytes (two words) into a long integer
    // this is NTP time (seconds since Jan 1 1900):
    unsigned long secsSince1900 = highWord << 16 | lowWord;
    Serial.print("Seconds since Jan 1 1900 = ");
    Serial.println(secsSince1900);
    // now convert NTP time into everyday time:
    Serial.print("Unix time = ");
    // Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
    const unsigned long seventyYears = 2208988800UL;
    // subtract seventy years:
    unsigned long epoch = secsSince1900 - seventyYears;
    // print Unix time:
    Serial.println(epoch);
    // calcular dia y hora actual
    if ((epoch % 86400L) / 3600 >= 5){
      hours = (epoch % 86400L) / 3600 - 5;
    }
    else{
      hours = (epoch % 86400L) / 3600 + 19;
    }
    minutes = (epoch % 3600) / 60;
    seconds = epoch % 60;
    ntpDia = (((epoch - 18000L) / 86400L) + 4) % 7;
    Serial.println(hours);
    Serial.println(minutes);
    Serial.println(seconds);
  }
}

unsigned long esp::sendNTPpacket(IPAddress &address){
  // send an NTP request to the time server at the given address
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011; // LI, Version, Mode
  packetBuffer[1] = 0;          // Stratum, or type of clock
  packetBuffer[2] = 6;          // Polling Interval
  packetBuffer[3] = 0xEC;       // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12] = 49;
  packetBuffer[13] = 0x4E;
  packetBuffer[14] = 49;
  packetBuffer[15] = 52;
  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  udp.beginPacket(address, 123); //NTP requests are to port 123
  udp.write(packetBuffer, NTP_PACKET_SIZE);
  udp.endPacket();
}

int esp::getHour(){
    return hours;
}

int esp::getMinutes(){
    return minutes;
}

int esp::getSeconds(){
    return seconds;
}

int esp::getDay(){
    return ntpDia;
}

boolean esp::conectedToWireless(){
    if (WiFi.status() == WL_CONNECTED){
        return true;
    }else{return false;}
}